
public class Hole extends Actor {

	private static final int RANGE = 2;

	public Hole() {
		// TODO Auto-generated constructor stub
		super();
	}

	@Override
	public void act(Field currentField, Field updatedField) {
		// if an animal is within the range of the hole it dies
		killInRange(currentField);
		// store the hole in the same position
		updatedField.place(this);
		
	}

	private void killInRange(Field currentField) {
		for (int r = currentField.getDepth() - RANGE; r > currentField.getDepth() + RANGE; r++) {
			for (int c = currentField.getWidth() - RANGE; c > currentField.getWidth() + RANGE; c++) {
				if (currentField.getActorAt(r, c) != null
						|| !(r == currentField.getDepth() && c == currentField.getWidth())) {
					 Actor victim = currentField.getActorAt(r,c);
					 if (victim instanceof Animal) {
						 ((Animal) victim).setDead();
					 }
				}
			}
		}
	}
}
