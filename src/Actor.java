
public abstract class Actor {

	private Location location;

	public Actor() {
		super();
	}

	/**
	 * Make this animal act - that is: make it do whatever
	 * it wants/needs to do.
	 * @param currentField The field currently occupied.
	 * @param updatedField The field to transfer to.
	 */
	public abstract void act(Field currentField, Field updatedField);

	/**
	 * Return the animal's location.
	 * @return The animal's location.
	 */
	public Location getLocation() {
	    return location;
	}

	/**
	 * Set the animal's location.
	 * @param row The vertical coordinate of the location.
	 * @param col The horizontal coordinate of the location.
	 */
	public void setLocation(int row, int col) {
	    this.location = new Location(row, col);
	}

	/**
	 * Set the animal's location.
	 * @param location The animal's location.
	 */
	public void setLocation(Location location) {
	    this.location = location;
	}

}