
// Added the following lines
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * A simple predator-prey simulator, based on a field containing rabbits and
 * foxes.
 * 
 * @author David J. Barnes and Michael Kolling
 * @version 2006.03.30
 * @modified Chuck Cusack, September, 2007
 * 
 */
public class Simulator {
	// Constants representing configuration information for the simulation.
	// The default width for the grid.
	private static final int DEFAULT_WIDTH = 50;
	// The default depth of the grid.
	private static final int DEFAULT_DEPTH = 50;
	// The probability that a fox will be created in any given grid position.
	private static final double FOX_CREATION_PROBABILITY = 0.02;
	// The probability that a rabbit will be created in any given grid position.
	private static final double RABBIT_CREATION_PROBABILITY = 0.08;
	// The probability that a hole will be created in any given grid position.
	private static final double HOLE_CREATION_PROBABILITY = 0.01;

	// The current state of the field.
	private Field field;
	// A second field, used to build the next stage of the simulation.
	private Field updatedField;
	// The current step of the simulation.
	private int step;
	// A graphical view of the simulation.
	private SimulatorView view;

	// Added this variable for use by the thread.
	private int numberSteps;

	// Added the following GUI stuff
	// The main window to display the simulation (and your buttons, etc.).
	JPanel buttonPanel;
	JButton runOneButton;
	JButton run100Button;
	JButton runLongButton;
	JButton resetButton;
	JTextField inputField;
	private JFrame mainFrame;
	private boolean resetHit;

	JMenuBar menuBar;

	// "File" and its items
	JMenu menuFile;
	JMenuItem fileItemExit;

	// "Help" and its items
	JMenu menuHelp;
	JMenuItem helpItemAbout;

	/**
	 * Construct a simulation field with default size.
	 */
	public Simulator() {
		this(DEFAULT_DEPTH, DEFAULT_WIDTH);
	}

	public static void main(String[] args) {
		// Create the simulator
		Simulator s = new Simulator();
	}

	/**
	 * Create a simulation field with the given size.
	 * 
	 * @param depth Depth of the field. Must be greater than zero.
	 * @param width Width of the field. Must be greater than zero.
	 */
	public Simulator(int depth, int width) {
		if (width <= 0 || depth <= 0) {
			System.out.println("The dimensions must be greater than zero.");
			System.out.println("Using default values.");
			depth = DEFAULT_DEPTH;
			width = DEFAULT_WIDTH;
		}
		field = new Field(depth, width);
		updatedField = new Field(depth, width);

		// Create a view of the state of each location in the field.
		view = new SimulatorView(depth, width);
		view.setColor(Rabbit.class, Color.orange);
		view.setColor(Fox.class, Color.blue);
		view.setColor(Hole.class, Color.red);

		// The rest of this method has changed. This sets up the
		// JFrame to display everything.

		mainFrame = new JFrame();

		mainFrame.setTitle("Fox and Rabbit Simulation");

		// Add window listener so it closes properly.
		// when the "X" in the upper right corner is clicked.
		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

		// Now, for the buttons...
		// Settings for the buttonPanel
		buttonPanel = new JPanel();

		// Add a button to run 1 steps.
		runOneButton = new JButton("Run 1 Step");
		buttonPanel.add(runOneButton);
		runOneButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				simulateOneStep();
			}
		});
		// Add a button to run 100 steps.
		run100Button = new JButton("Run 100 Steps");
		buttonPanel.add(run100Button);
		run100Button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				simulate(100);
			}
		});
		// Add a button to run long simulation.
		runLongButton = new JButton("Run Long Sim");
		buttonPanel.add(runLongButton);
		runLongButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runLongSimulation();
			}
		});
		// Add a reset button
		resetButton = new JButton("Reset");
		buttonPanel.add(resetButton);
		resetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				reset();
			}
		});

		// Add the input field
		inputField = new JTextField(15);
		buttonPanel.add(inputField);
		inputField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String inputValue = inputField.getText();
				try {
					simulate(Integer.parseInt(inputValue));
				} catch (NumberFormatException f) {
					inputField.setText("Please enter a whole number.");
				}
			}
		});

		// Add a menu Bar
		menuBar = new JMenuBar();

		// "File" menu
		menuFile = new JMenu("File");
		fileItemExit = new JMenuItem("Exit");
		fileItemExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		// "Help" menu
		menuHelp = new JMenu("Help");
		helpItemAbout = new JMenuItem("About");
		String aboutString = "Author: Daniel Keith\nDate: 02/01/2019";
		helpItemAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(mainFrame, aboutString, "About", 2);
			}
		});

		menuFile.add(fileItemExit);
		menuHelp.add(helpItemAbout);
		menuBar.add(menuFile);
		menuBar.add(menuHelp);

		// Adding things to a JFrame.
		Container contents = mainFrame.getContentPane();
		contents.add(view, BorderLayout.CENTER);
		contents.add(buttonPanel, BorderLayout.NORTH);
		mainFrame.setJMenuBar(menuBar);

		// You always need to call pack on a JFrame.
		mainFrame.pack();

		// For very subtle reasons, this MUST go after the pack statement above.
		// Just trust me on this one. Or figure out why for yourself.
		reset();

		// You always need to call setVisible(true) on a JFrame.
		mainFrame.setVisible(true);
	}

	/**
	 * Run the simulation from its current state for a reasonably long period, e.g.
	 * 500 steps.
	 */
	public void runLongSimulation() {
		simulate(500);
	}

	/**
	 * Run the simulation from its current state for the given number of steps. Stop
	 * before the given number of steps if it ceases to be viable. This has been
	 * modified so it uses a thread--this allows it to work in conjunction with
	 * Swing. Modified by Chuck Cusack, Sept 18, 2007
	 * 
	 * @param numSteps How many steps to run for.
	 */
	public void simulate(int numSteps) {
		// For technical reason, I had to add numberSteps as a class variable.
		numberSteps = numSteps;
		// Create a thread
		Thread runThread = new Thread() {
			// When the thread runs, it will simulate numberSteps steps.
			public void run() {
				// Disable the button until the simulation is done.
				runOneButton.setEnabled(false);
				run100Button.setEnabled(false);
				runLongButton.setEnabled(false);
				inputField.setEnabled(false);
				for (int step = 1; step <= numberSteps && view.isViable(field); step++) {
					if (resetHit) {
						runOneButton.setEnabled(true);
						run100Button.setEnabled(true);
						runLongButton.setEnabled(true);
						inputField.setEnabled(true);
						resetHit = false;
						this.stop();
					} else {
						simulateOneStep();
					}
				}
				// Now re-enable the button
				runOneButton.setEnabled(true);
				run100Button.setEnabled(true);
				runLongButton.setEnabled(true);
				inputField.setEnabled(true);
			}
		};
		// Start the thread
		runThread.start();
		// Now this method exits, allowing the GUI to update. The simulation is being
		// run on a different thread, so the GUI updates as the simulation continues.
	}

	/**
	 * Run the simulation from its current state for a single step. Iterate over the
	 * whole field updating the state of each fox and rabbit.
	 */
	public void simulateOneStep() {
		step++;

		// let all animals act
		ArrayList<Actor> actors = field.getActors();
		Collections.shuffle(actors); // to randomize the order they act.
		for (Iterator<Actor> it = actors.iterator(); it.hasNext();) {
			Actor actor = it.next();
			actor.act(field, updatedField);
		}

		// Swap the field and updatedField at the end of the step.
		Field temp = field;
		field = updatedField;
		updatedField = temp;
		updatedField.clear();

		// Display the new field on screen.
		view.showStatus(step, field);
	}

	/**
	 * Reset the simulation to a starting position.
	 */
	public void reset() {
		resetHit = true;
		step = 0;
		field.clear();
		updatedField.clear();
		populate(field);

		// Show the starting state in the view.
		view.showStatus(step, field);
		resetHit = false;
	}

	/**
	 * Populate a field with foxes, rabbits, and holes.
	 * 
	 * @param field The field to be populated.
	 */
	private void populate(Field field) {
		Random rand = new Random();
		field.clear();
		for (int row = 0; row < field.getDepth(); row++) {
			for (int col = 0; col < field.getWidth(); col++) {
				if (rand.nextDouble() <= HOLE_CREATION_PROBABILITY) {
					Hole hole = new Hole();
					hole.setLocation(row, col);
					field.place(hole);
				} else if (rand.nextDouble() <= FOX_CREATION_PROBABILITY) {
					Fox fox = new Fox(true);
					fox.setLocation(row, col);
					field.place(fox);
				} else if (rand.nextDouble() <= RABBIT_CREATION_PROBABILITY) {
					Rabbit rabbit = new Rabbit(true);
					rabbit.setLocation(row, col);
					field.place(rabbit);
				}
				// else leave the location empty.
			}
		}
	}
}
